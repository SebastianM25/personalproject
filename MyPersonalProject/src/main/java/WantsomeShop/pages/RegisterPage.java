package WantsomeShop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegisterPage {

    private static final By email_Field = By.id("reg_email");
    private static final By password_Field = By.id("reg_password");
    private static final By firstName_Field = By.id("registration_field_1");
    private static final By lastName_Field = By.id("registration_field_2");
    private static final By phone_Field = By.id("registration_field_3");
    private static final By register_Btn = By.xpath("//input[@name='register']");

    WebDriver driver;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fillEmail(String email) {
        driver.findElement(email_Field).sendKeys(email);
    }

    public void fillPassword(String password) {

        driver.findElement(password_Field).sendKeys(password);
    }

    public void fillFirstName(String firstName) {

        driver.findElement(firstName_Field).sendKeys(firstName);
    }

    public void fillLastName(String lastName) {

        driver.findElement(lastName_Field).sendKeys(lastName);
    }

    public void fillPhone(String phone) {

        driver.findElement(phone_Field).sendKeys(phone);
    }

    public void clickRegister() {

        driver.findElement(register_Btn).click();
    }
}
