package WantsomeShop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Comment {
    private static final By commentButton= By.xpath("//article[@id='post-244']//a[contains(text(),'0 Comment')]");
    private static final By blogButton= By.xpath("//a[contains(text(),'Blog')]");
    WebDriver driver;
    public Comment(WebDriver driver) {
        this.driver = driver;
    }

    public void clickBlog(){
        driver.findElement(blogButton).click();
    }

    public void clickComment(){
        driver.findElement(commentButton).click();
    }
}
