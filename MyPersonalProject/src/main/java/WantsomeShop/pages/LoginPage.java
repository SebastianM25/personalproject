package WantsomeShop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    private static final By username_Field = By.xpath("//input[@id='username']");
    private static final By password_Field = By.id("password");
    private static final By loginButton = By.xpath("//input[@name='login']");
    private static final By rememberMe = By.id("rememberme");
    private static final By lastPassword = By.xpath("//a[contains(text(),'Lost your password?')]");
    private static final By fieldForLostPassword=By.xpath("//input[@id='user_login']");
    private static final By resetPasswordButton=By.xpath("//input[@class='woocommerce-Button button']");
    private static final By logoutButton=By.xpath("//a[contains(text(),'Sign out')]");
    WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fillUsername(String username) {

        driver.findElement(username_Field).sendKeys(username);
    }

    public void fillPassword(String password) {

        driver.findElement(password_Field).sendKeys(password);
    }

    public void clickOnLoginButton() {
        driver.findElement(loginButton).click();
    }
    public void clickOnLostPassword(){
        driver.findElement(lastPassword).click();
    }
    public void clickOnFieldLostPassword(String fieldLostPassword){
        driver.findElement(fieldForLostPassword).sendKeys(fieldLostPassword);
    }
    public void resetPassword(){
        driver.findElement(resetPasswordButton).click();
    }
    public void clickOnLogoutButton(){
        driver.findElement(logoutButton).click();
    }

}
