package DataDrivenTest;

import WantsomeShop.Base.BaseClass;
import WantsomeShop.pages.LoginPage;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RunWith(DataProviderRunner.class)
public class LoginTest_WithDataDriven extends BaseClass {
    @UseDataProvider("powers")
    @Test()
    public void testLoginWithDataDriven(String username, String password) throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        /*String username = "gmail@gmail.com";
        String password = "Sebastian122!@";*/
        navigateToLoginPage();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.fillUsername(username);
        loginPage.fillPassword(password);
        loginPage.clickOnLoginButton();
        Thread.sleep(5000);
    }

    @DataProvider()
    public static Object[][] powers() throws IOException {
        List<Object[]> outputList = new ArrayList<>();
        List<String> records = readAllLinesFromResourcesFile("DataDriven/user_db.csv");
        for (String record : records) {

            String[] params = record.split(",");
            outputList.add(params);
        }
        Object[][] result = new Object[records.size()][];
        outputList.toArray(result);
        return result;
    }


    public void navigateToLoginPage() {
        driver.findElement(By.xpath("//a[@class='user-icon']")).click();
    }

    public static List<String> readAllLinesFromResourcesFile(String fileName) throws IOException {
        List<String> result = new ArrayList<>();
        ClassLoader classLoader = LoginTest_WithDataDriven.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        }
        return result;
    }
}
