package Tests;

import WantsomeShop.Base.BaseClass;
import WantsomeShop.pages.RegisterPage;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

public class Register_TestCases extends BaseClass {
    @Test
    public void testValidRegister() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        String email = "test.automation183@gmail.com";
        String password = "Sebastian17!";
        String firstName = "Popescu";
        String lastName = "Ionescu";
        String phone = "0760898493";
        navigateToLoginPage();
        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.fillEmail(email);
        registerPage.fillPassword(password);
        registerPage.fillFirstName(firstName);
        registerPage.fillLastName(lastName);
        registerPage.fillPhone(phone);
        registerPage.clickRegister();
        Thread.sleep(5000);
    }

    public void navigateToLoginPage() {
        driver.findElement(By.xpath("//a[@class='user-icon']")).click();
    }
}
