package Tests;

import WantsomeShop.Base.BaseClass;
import WantsomeShop.pages.LoginPage;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class Login_TestCases extends BaseClass {

    @Test
    public void testValidLogin() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        String username = "gmail@gmail.com";
        String password = "Sebastian122!@";
        navigateToLoginPage();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.fillUsername(username);
        loginPage.fillPassword(password);
        loginPage.clickOnLoginButton();
    }

    @Test
    public void testInvalidLogin() throws InterruptedException {
        String username = "nou@gmail.com";
        String password = "Sebastian122!@";
        navigateToLoginPage();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.fillUsername(username);
        loginPage.fillPassword(password);
        loginPage.clickOnLoginButton();
        assertTrue(driver.getPageSource().contains("A user could not be found with this email address."));
        Thread.sleep(5000);
    }

    @Test
    public void checkTheMessageForPasswordField() throws InterruptedException {
        String username = "nou@gmail.com";
        navigateToLoginPage();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.fillUsername(username);
        loginPage.clickOnLoginButton();
        assertTrue(driver.getPageSource().contains("A user could not be found with this email address."));
        Thread.sleep(5000);
    }

    @Test
    public void checkLostPassword() throws InterruptedException {
        String fieldLostPassword = "test.automation183@gmail.com";
        navigateToLoginPage();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickOnLostPassword();
        loginPage.clickOnFieldLostPassword(fieldLostPassword);
        loginPage.resetPassword();
        Thread.sleep(10000);
    }

    public void navigateToLoginPage() {
        driver.findElement(By.xpath("//a[@class='user-icon']")).click();
    }
}
