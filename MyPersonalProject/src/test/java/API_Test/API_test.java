package API_Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.get;
import static org.junit.Assert.assertEquals;

public class API_test {
    @Test
    public void checkResponse(){
        get("https://practica.wantsome.ro/shop/")
                .then()
                .assertThat()
                .statusCode(200);
    }
    @Test
    public void testCorrectRequest(){
        Response response=RestAssured.get("https://practica.wantsome.ro/shop/");
        int code=response.getStatusCode();
        System.out.println("Status code is: "+code);
        assertEquals(code,200);
    }
    @Test
    public void testIncorrectRequest(){
        Response response= RestAssured.get("https://practica.wantsome.ro/shop/");
        int code=response.getStatusCode();
        System.out.println("Status code is: "+code);
        assertEquals(code,400);
    }
    @Test
    public void checkResponseTime(){
        Response response= RestAssured.get("https://practica.wantsome.ro/shop/");
        System.out.println("Response time is: "+response.getTimeIn(TimeUnit.MILLISECONDS));
        System.out.println("Response time is: "+response.getTimeIn(TimeUnit.SECONDS));
    }
}
